// Case-to-Case Cell-Croosing Count  

#include<iostream.h>
#include <stdio.h>
#include <search.h>
#include <malloc.h>
#include <math.h>
#include "VoronoiDiagramGenerator.h"
#include <time.h>

#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836
#define MASK 123459876
#define MAX0 1000
#define MAX 10000
#define MAX2 100000

float rnd(long idum0);
/*=============================================================================*/
/*As linhas abaixo controlam os par�metros da simula��o*/
/*=============================================================================*/
int ff=1;//0:N�o escreve nada;  1:Escreve s� resultado; 
         //2:Escreve mais coisas; 3:Escreve tudo.
int peer=1;//rivaliza peso com distancia euclidiana
int pf=1;//print flag. 
int pff=1;//print flag para escrever clusters solucoes.  
long nsim;//numero de simulacoes
long nord=5, npar=9, npar0;
//nord=#pontos proximos por dist. euclideana.
//npar=#pontos proximos por Pareto no espa�o distancia euclideana versus angulo.
int fo45=1;//1:Faz grafo completo; 0:Faz grafo reduzido por Pareto.
int ncl=0;//#clusters;
long idum=1129;
int ftg=0;
int fmediaarestas;
//0: Usa a menor aresta;
//1: Usa m�dia das arestas para o diametro do c�rculo em cada caso;  
//2: Usa a maior aresta;
//3: Usa a media das arestas retirando-se a maior e a menor aresta (se grau>2).
//4: Usa a media das arestas retirando-se a maior e a menor aresta (se grau>2) 
//                           e usa a maior aresta se grau<=2.
long fraccasmax;
int tcmax;
//double fracpopmax=0.20;
/*============================================================================= */

long kke=0;
double pi=3.141592653587; double pi2,pid2;
float lix,liy,lsx,lsy,xx,yy;
long count,count1,i,j;
float xValues[MAX];
float yValues[MAX];
long li[MAX2][5],//aresta(vertice_a,vertice_b),peso,caso_a,caso_b, para casos.
     nli,ni,nc,jj;
double lide[MAX],distcas[MAX][MAX];
int icas[MAX],icasa[MAX];
int cluster[MAX0][MAX0],ncluster[MAX0];
long asoma[MAX0],scluster[MAX0];
double xxx[MAX2],yyy[MAX2];
long rxxx[MAX2],ryyy[MAX2];
double theta[MAX0],dista[MAX0],dista0[MAX0]; long idista[MAX0],idista2[MAX0]; //ordena5

double theta2[MAX0],dista2[MAX0]; long itheta2[MAX0]; //ordena4
long itheta[MAX0];//ordena6
double thetan[MAX0]; long ithetan[MAX0]; //ordena7
int efpareto[MAX0],levelefpareto[MAX0],markpareto[MAX0];
char micas[MAX0][MAX0];
double entrop[MAX0],dentrop[MAX0];
int identrop[MAX0]; //ordena7
double entropia,entropia2,deltaentrop;
double f1[MAX0],f2[MAX0];
int pareto[MAX0],ipareto[MAX0];
int indice[MAX0],indicejj;
int somaarestas[MAX0],minarestas[MAX0],maxarestas[MAX0];
double mediaarestas[MAX0];
double popscan=0.0, casscan, mu, cas, scan, sig=0.0;
long Tp=0, nclreal=0, ncldetect=0;
double sens, ppv;
double sensmedia, ppvmedia;
//long vj[MAX0];
double LLRcrit[MAX];
float jsim;

FILE *ent;
FILE *sai;
FILE *ent2;
FILE *vor;
FILE *solucoes;
FILE *fftg;
FILE *sai3;
FILE *so;
FILE *so2;
FILE *so3;
FILE *sof;
FILE *sai4;
FILE *llrcrit;

typedef struct TipoAresta
{
   int u, v, peso;
   float eucl;
};
TipoAresta Aresta[MAX2];  
int Arvore[MAX][3], vertice[MAX2],qvertice[MAX],cvertice[MAX];
int k,n;
  

double g(double x){
   return(x*x);
}  

// model Beernoulli
float bernoulli(double c, double n, int C, int N){
      double L,Ec;
      Ec=n*(float)C/N;
      if(c/n>(C-n)/(N-n)){
          if (c!=n){
             L=c*log(c/n)+(n-c)*log((n-c)/n)+(C-c)*log((C-c)/(N-n))+(N-n-C+c)*log((N-n-C+c)/(N-n))
             -(C*log(C)+(N-C)*log(N-C)-N*log(N));
          }
          else{
             L=c*log(c/n)+(C-c)*log((C-c)/(N-n))+(N-n-C+c)*log((N-n-C+c)/(N-n))
             -(C*log(C)+(N-C)*log(N-C)-N*log(N));   
          }
      }
      else  L=0;
      return(L);
}
// model Poisson
float poisson(double c, double n, int C, int N){
      double L,Ec;
      Ec=n*(float)C/N;
      //if(c>Ec) 
      L=c*log(c/Ec)+(C-c)*log((C-c)/(C-Ec));
      //else L=0;
      return(L);
}

/*============================================================================*/
/*Fun��es de ordena��o*/
/*=============================================================================*/

void particao(long esq, long dir, long *i, long *j){
     if(peer){//flag para desempate usando distancia euclidiana
         float pivot;
         //long tempi;
         TipoAresta temp;
         //int i,j;
         *i=esq;
         *j=dir;
         //pivot=Aresta[((*i)+(*j))/2].peso; //encontra valor do pivot
         pivot=Aresta[((*i)+(*j))/2].eucl;
         do{
           //while (Aresta[*i].peso<pivot)
           while (Aresta[*i].eucl<pivot)
             (*i)++;
           //while (Aresta[*j].peso>pivot)
           while (Aresta[*j].eucl>pivot)
             (*j)--;
           if ((*i)<=(*j)){       
             temp = Aresta[*i];
             Aresta[*i] = Aresta[*j];
             Aresta[*j] = temp;
             //coloca Aresta[] em ordem crescente de Aresta[].eucl
             (*i)++;
             (*j)--;
           }
         }while ((*i)<=(*j));
     }     
     else {
         long pivot;
         //long tempi;
         TipoAresta temp;
         //int i,j;
         *i=esq;
         *j=dir;
         pivot=Aresta[((*i)+(*j))/2].peso; //encontra valor do pivot         
         do{
           while (Aresta[*i].peso<pivot)           
             (*i)++;
           while (Aresta[*j].peso>pivot)         
             (*j)--;
           if ((*i)<=(*j)){       
             temp = Aresta[*i];
             Aresta[*i] = Aresta[*j];
             Aresta[*j] = temp;
             //coloca Aresta[] em ordem crescente de Aresta[].peso
            
             (*i)++;
             (*j)--;
           }
         }while ((*i)<=(*j));
     }
     
}//fim de particao
void ordena(long esq, long dir){
      long i,j;
      particao(esq,dir,&i,&j);
      if (esq<j)
        ordena(esq,j);
      if (i<dir)
        ordena(i,dir);
}//fim de ordena


void particaoxxx(long esq, long dir, long *i, long *j){
     double pivot;
     long tempi;
     double temp;
     //int i,j;
     *i=esq;
     *j=dir;
     pivot=xxx[((*i)+(*j))/2]; //encontra valor do pivot
     do{
       while (xxx[*i]<pivot)
         (*i)++;
       while (xxx[*j]>pivot)
         (*j)--;
       if ((*i)<=(*j)){       
         temp = xxx[*i];
         xxx[*i] = xxx[*j];
         xxx[*j] = temp;
         temp = yyy[*i];
         yyy[*i] = yyy[*j];
         yyy[*j] = temp;
         //coloca xxx[] em ordem crescente
         tempi=rxxx[*i];
         rxxx[*i]=rxxx[*j];
         rxxx[*j]=tempi;
         (*i)++;
         (*j)--;
       }
     }while ((*i)<=(*j));
}//fim de particaoxxx
void ordenaxxx(long esq, long dir){
      long i,j;
      particaoxxx(esq,dir,&i,&j);
      if (esq<j)
        ordenaxxx(esq,j);
      if (i<dir)
        ordenaxxx(i,dir);
}//fim de ordenaxxx


void particao3(long esq, long dir, long *i, long *j){//coloca thetan[] em ordem decrescente
     double pivot;
     long tempi;
     double temp;
     //int i,j;
     *i=esq;
     *j=dir;
     pivot=thetan[((*i)+(*j))/2]; //encontra valor do pivot
     do{
       while (thetan[*i]<pivot)
         (*i)++;
       while (thetan[*j]>pivot)
         (*j)--;
       if ((*i)<=(*j)){       
         temp = thetan[*i];
         thetan[*i] = thetan[*j];
         thetan[*j] = temp;
         
         tempi=ithetan[*i];
         ithetan[*i]=ithetan[*j];
         ithetan[*j]=tempi;
         (*i)++;
         (*j)--;
       }
     }while ((*i)<=(*j));
}//fim de particao3
void ordena3(long esq, long dir){//coloca thetan[] em ordem decrescente
      long i,j;
      particao3(esq,dir,&i,&j);
      if (esq<j)
        ordena3(esq,j);
      if (i<dir)
        ordena3(i,dir);
}//fim de ordena3


void particao4(long esq, long dir, long *i, long *j){//coloca theta2[] em ordem crescente
     double pivot;
     long tempi;
     double temp;
     *i=esq;
     *j=dir;
     pivot=theta2[((*i)+(*j))/2]; //encontra valor do pivot
     do{
       while (theta2[*i]<pivot)
         (*i)++;
       while (theta2[*j]>pivot)
         (*j)--;
       if ((*i)<=(*j)){       
         temp = theta2[*i];
         theta2[*i] = theta2[*j];
         theta2[*j] = temp;
         
         temp = dista2[*i];
         dista2[*i] = dista2[*j];
         dista2[*j] = temp;
        
         tempi = itheta2[*i];
         itheta2[*i] = itheta2[*j];
         itheta2[*j] = tempi;
         
         (*i)++;
         (*j)--;
       }
     }while ((*i)<=(*j));
}//fim de particao4
void ordena4(long esq, long dir){//coloca theta2[] em ordem crescente
      long i,j;
      particao4(esq,dir,&i,&j);
      if (esq<j)
        ordena4(esq,j);
      if (i<dir)
        ordena4(i,dir);
}//fim de ordena4


void particao5(long esq, long dir, long *i, long *j){//coloca dista[] em ordem crescente
     double pivot;
     long tempi;
     double temp;
     *i=esq;
     *j=dir;
     pivot=dista[((*i)+(*j))/2]; //encontra valor do pivot
     do{
       while (dista[*i]<pivot)
         (*i)++;
       while (dista[*j]>pivot)
         (*j)--;
       if ((*i)<=(*j)){       
         temp = dista[*i];
         dista[*i] = dista[*j];
         dista[*j] = temp;
         
         temp = theta[*i];
         theta[*i] = theta[*j];
         theta[*j] = temp;
        
         tempi = idista[*i];
         idista[*i] = idista[*j];
         idista[*j] = tempi;
         
         tempi = indice[*i];
         indice[*i] = indice[*j];
         indice[*j] = tempi;
         
         (*i)++;
         (*j)--;
       }
     }while ((*i)<=(*j));
}//fim de particao5
void ordena5(long esq, long dir){//coloca dista[] em ordem crescente
      long i,j;
      particao5(esq,dir,&i,&j);
      if (esq<j)
        ordena5(esq,j);
      if (i<dir)
        ordena5(i,dir);
}//fim de ordena5


void particao6(long esq, long dir, long *i, long *j){//coloca theta[] em ordem crescente
     double pivot;
     long tempi;
     double temp;
     *i=esq;
     *j=dir;
     pivot=theta[((*i)+(*j))/2]; //encontra valor do pivot
     do{
       while (theta[*i]<pivot)
         (*i)++;
       while (theta[*j]>pivot)
         (*j)--;
       if ((*i)<=(*j)){       
         temp = theta[*i];
         theta[*i] = theta[*j];
         theta[*j] = temp;
 
         tempi = itheta[*i];
         itheta[*i] = itheta[*j];
         itheta[*j] = tempi;
         
         tempi = indice[*i];
         indice[*i] = indice[*j];
         indice[*j] = tempi;
         
         (*i)++;
         (*j)--;
       }
     }while ((*i)<=(*j));
}//fim de particao6
void ordena6(long esq, long dir){//coloca theta[] em ordem crescente
      long i,j;
      particao6(esq,dir,&i,&j);
      if (esq<j)
        ordena6(esq,j);
      if (i<dir)
        ordena6(i,dir);
}//fim de ordena6


void particao7(long esq, long dir, long *i, long *j){//coloca thetan[] em ordem DEcrescente
     double pivot;
     long tempi;
     double temp;
     *i=esq;
     *j=dir;
     pivot=thetan[((*i)+(*j))/2]; //encontra valor do pivot
     do{
       while (thetan[*i]>pivot)
         (*i)++;
       while (thetan[*j]<pivot)
         (*j)--;
       if ((*i)<=(*j)){       
         temp = thetan[*i];
         thetan[*i] = thetan[*j];
         thetan[*j] = temp;
         
         tempi = ithetan[*i];
         ithetan[*i] = ithetan[*j];
         ithetan[*j] = tempi;
         
         temp = dentrop[*i];
         dentrop[*i] = dentrop[*j];
         dentrop[*j] = temp;

         tempi = indice[*i];
         indice[*i] = indice[*j];
         indice[*j] = tempi;
        
         (*i)++;
         (*j)--;
       }
     }while ((*i)<=(*j));
}//fim de particao7
void ordena7(long esq, long dir){//coloca thetan[] em ordem DEcrescente
      long i,j;
      particao7(esq,dir,&i,&j);
      if (esq<j)
        ordena7(esq,j);
      if (i<dir)
        ordena7(i,dir);
}//fim de ordena7



void particao8(long esq, long dir, long *i, long *j){//coloca pareto[] em ordem crescente
     int pivot;
     long tempi;
     double temp;
     *i=esq;
     *j=dir;
     pivot=pareto[((*i)+(*j))/2]; //encontra valor do pivot
     do{
       while (pareto[*i]<pivot)
         (*i)++;
       while (pareto[*j]>pivot)
         (*j)--;
       if ((*i)<=(*j)){       
         
         tempi = pareto[*i];
         pareto[*i] = pareto[*j];
         pareto[*j] = tempi;
 
         tempi = indice[*i];
         indice[*i] = indice[*j];
         indice[*j] = tempi;
         
         (*i)++;
         (*j)--;
       }
     }while ((*i)<=(*j));
}//fim de particao8
void ordena8(long esq, long dir){//coloca pareto[] em ordem crescente
      long i,j;
      particao8(esq,dir,&i,&j);
      if (esq<j)
        ordena8(esq,j);
      if (i<dir)
        ordena8(i,dir);
}//fim de ordena8
/*=============================================================================*/
//funcoes (particao e ordena) que ordenam os dados (quick sort).
void particaollr(long esq, long dir, long *i, long *j)
{
	double temp,pivot;
	int tempi;
	*i=esq;
	*j=dir;
	
	pivot=LLRcrit[((*i)+(*j))/2]; //encontra valor do pivot
	do
	{
		while (LLRcrit[*i]<pivot) //> ordem decrescente.
		(*i)++;
		while (LLRcrit[*j]>pivot) //< ordem decrescente.
		(*j)--;
		if ((*i)<=(*j))
		{
			temp=LLRcrit[*i]; //troca distancias.
			LLRcrit[*i]=LLRcrit[*j];
			LLRcrit[*j]=temp;
			
			(*i)++;
			(*j)--;
		}
	}while ((*i)<=(*j));
}//fim de particaollr

void ordenallr(long esq, long dir)
{
	long i,j;
	particaollr(esq,dir,&i,&j);
	if (esq<j)
    ordenallr(esq,j);
	if (i<dir)
	ordenallr(i,dir);
}//fim de ordenallr
/*=============================================================================*/

/*=============================================================================*/
/*Funcoes para a construcao da Arvore Geradora Minima (pela m�trica C-5.0)*/
/*=============================================================================*/
int LeArestasVetorli() //Formato vetor li[][]
{   
    n=nc;
    k = 1;
	int i, j;
	for (i = 0; i < nli; i++){
            {
			  Aresta[k].u = li[i][3]; //Varia de 1 a nc
			  Aresta[k].v = li[i][4]; //Varia de 1 a nc
			  Aresta[k].peso = li[i][2];
			  Aresta[k].eucl = lide[i];//desempata os pesos das arestas pela distancia euclidiana
			  k++;
            }  
        }	
	k--;
//	if(pf>2){
//    for (i = 1; i <= k; i++){ 	
//       cout<<"\n" << Aresta[i].u << " : " << Aresta[i].v << " : " << Aresta[i].peso << " : " << Aresta[i].eucl <<"\n";	
//       cout << "\n";}system("pause");
//    }   
    return (k);
}

int find(int vertex)
{
	return (vertice[vertex]);
}

void Acresc(int v1, int v2)
{
	int i, j,vv1,vv2;
	vv1=vertice[v1];
	vv2=vertice[v2];
	if(vertice[v1]<vertice[v2]){
       for(i=1;i<=n;i++){
          if(vertice[i]==vv2)vertice[i]=vv1;
       }                               
    }  
    else{//vertice[v2]<vertice[v1]
       for(i=1;i<=n;i++){
          if(vertice[i]==vv1)vertice[i]=vv2;
       }    
    }                               
}

void OrdenaArestas(int k)
{
	int i, j;
	struct TipoAresta temp;
	if(pf>2)cout << "original:\n";  
	if(pf>2)for (i = 1; i <=k; i++) 	
       cout << Aresta[i].u << " : " << Aresta[i].v << " : " << Aresta[i].peso << " : " << Aresta[i].eucl <<"\n";
    if(pf>2)cout << "\n";   
    ordena(1,k);//ordena por quicksort
	if(pf>1)cout << "ordenado:\n"; 		
	if(pf>1)for (i = 1; i <= k; i++) 	
       cout << Aresta[i].u << " : " << Aresta[i].v << " : " << Aresta[i].peso << " : " << Aresta[i].eucl << "\n";	
    if(pf>1)cout << "\n";   
    //system("pause");
}

int ArvoreGM(int k)
{
	int i, t, soma,soma2,j;	
	OrdenaArestas(k);
	t = 1;
	soma = 0; soma2=0;
	if(pf>2)cout << "k=" << k << "\n";   
	
	if(pf>2)for (i = 1; i <=k; i++) 	
       cout << "u=" << Aresta[i].u << " v=" << Aresta[i].v << " w=" << Aresta[i].peso << "\n";
    if(pf>2)cout << "\n";   
    i=0;
    do{
        i++; 
        if(pf>1)for (j = 1; j <=n; j++){ 	
          cout << vertice[j] << ":";
        }  
        if(pf>1)cout << "   u=" << Aresta[i].u << " v=" << Aresta[i].v << "\n";  
		if (find (Aresta[i].u) != find (Aresta[i].v))
		{
			Arvore[t][0] = Aresta[i].u;
			Arvore[t][1] = Aresta[i].v;
			Arvore[t][2] = Aresta[i].peso;
			if(pf>1)cout<<"     u="<<Aresta[i].u<<" v=" << Aresta[i].v << " w=" << Aresta[i].peso << "\n";
			soma += Aresta[i].peso;
			Acresc (Aresta[i].u, Aresta[i].v);
			t++;
		}
    }while(i<k && t<n);	
    if(pf>1)for (j = 1; j <=n; j++){ 	
          cout << vertice[j] << ":";
    }  
    if(pf>1)cout << "\n";   
	for (i = 1; i < n; i++){
		if(pf>1)cout << Arvore[i][0] << " - " << Arvore[i][1] << " : " << Arvore[i][2] << endl;
		soma2 += Arvore[i][2];
    }	
	return soma;
}

void display(int custo)
{
	int  i;
	if(pf>1){
       cout<<n<<" vertices\n"; //fprintf(so,"%d vertices\n",n);
//       cout << "\n"<<n-1<<" arestas da Arvore Geradora miniima:\n\n";
//       fprintf(so,"\n %d arestas da Arvore Geradora Minima:\n\n",n-1);
    }   
	fprintf(vor,"%d\n",nc-1);
	for (i = 1; i <= n; i++){
      somaarestas[i]=0;
      minarestas[i]=3000;
      maxarestas[i]=0;  
    }    
	for (i = 1; i < n; i++){
		if(pf>1){
          cout<<Arvore[i][0]<<"-"<<Arvore[i][1]<<":"<<Arvore[i][2]<<endl;
        }
        //fprintf(so,"%d-%d:%d\n",Arvore[i][0],Arvore[i][1],Arvore[i][2]);
        somaarestas[Arvore[i][0]]+=Arvore[i][2];
        somaarestas[Arvore[i][1]]+=Arvore[i][2];
        if(Arvore[i][2] < minarestas[Arvore[i][0]]) minarestas[Arvore[i][0]]=Arvore[i][2];
        if(Arvore[i][2] < minarestas[Arvore[i][1]]) minarestas[Arvore[i][1]]=Arvore[i][2];
        if(maxarestas[Arvore[i][0]]<Arvore[i][2])  maxarestas[Arvore[i][0]]=Arvore[i][2];
        if(maxarestas[Arvore[i][1]]<Arvore[i][2])  maxarestas[Arvore[i][1]]=Arvore[i][2];
		fprintf(vor,"%f %f %f %f %d\n",xValues[icas[Arvore[i][0]-1]],
                                    yValues[icas[Arvore[i][0]-1]],
                                    xValues[icas[Arvore[i][1]-1]],
                                    yValues[icas[Arvore[i][1]-1]],
                                    Arvore[i][2]);  
    }	
    
	if(pf>1){
       cout << "\nCusto da Arvore Geradora Minima: " << custo << endl;
//	   fprintf(so,"\nCusto da Arvore Geradora Minima: %d\n",custo);
    }   
}
/*============================================================================*/


/*============================================================================*/
/* Procura um valor x num array v de tamanho n previamente ordenado. Retorna o 
�ndice de uma ocorr�ncia de x em v, se encontrar; sen�o, retorna -1. Sup�e que 
os elementos do array s�o compar�veis com os operadores de compara��o.*/
/*============================================================================*/
template <class T>
int BinarySearch(const T v[], int n, T x)
{
    int left = 0, right = n - 1;
    while (left <= right){
       int middle = (left + right) / 2;
       if (x == v[middle])
          return middle; // encontrou
       else if (x > v[middle])
          left = middle + 1;
       else
          right = middle - 1;
    }
    return -1; // n�o encontrou
}
/*=============================================================================*/ 


int main(int argc,char **argv) 
{	
    clock_t start, end;
    double cpu_time_used;
    
    double voronoi[60000][4];  
    long lit[MAX0][5],litnext[MAX0],litprev[MAX0];
    long pertencepareto[MAX0];
    double delmax;
    
    long ni;
    double x3,y3,x4,y4,den,ua,ub,x,y;
    double x43,y43,x13,y13,x21,y21;
    double xmina,xminv,xmaxa,xmaxv,ymina,yminv,ymaxa,ymaxv;     

    double LLRmax=0.0,LLRmax2=0.0,LLRmax3=0.0,LLRmax0; long iLLRmax,iLLRmax2,iLLRmax3; 
    
    long a,k1,pins1,jj1;
    pi2=pi*2.0; pid2=pi/2.0;
    
    for(j=0;j<MAX0;j++)pertencepareto[j]=1;
    
    if(pff)solucoes=fopen("solucoes.txt","w");
    so=fopen("so.txt","w");
    so2=fopen("so2.txt","w");
    so3=fopen("so3.txt","w");
    sof=fopen("sof.txt","w");
    llrcrit=fopen("llrcrit.txt","w");
  
      
    sai=fopen("saida.txt","w");      
    sai3=fopen("file_grafico.txt","w");
    sai4=fopen("p-valor.txt","w");
    
    ent2=fopen("entrada.txt","r");
    fscanf(ent2,"%d",&fmediaarestas);
    fscanf(ent2,"%d",&fraccasmax);
    fscanf(ent2,"%d",&nsim);    
    ent=fopen("casos.txt","r");
    /*=========================================================================*/
    //Entrada cont�m coordenadas dos individuos, 1:casos, 0:controle
    /*=========================================================================*/
    count=0; nc=0; lix=liy=1e30; lsx=lsy=-1e30;
    do{
       fscanf(ent,"%f",&xx); xValues[count]=xx;//+count/2000.0;//+rnd(idum);
       fscanf(ent,"%f",&yy); yValues[count]=yy;//+count/2000.0;//+rnd(idum);
       fscanf(ent,"%d",&j);  
       if(j!=-1){
         if(j==1){icas[nc]=count; nc++;}
         if(xx<lix)lix=xx;
         if(xx>lsx)lsx=xx;
         if(yy<liy)liy=yy;
         if(yy>lsy)lsy=yy;
         //fprintf(sai,"%f %f\n",xValues[count],yValues[count]);
         count++;
         //cout<<count<<" ";
       }  
    }while(j!=-1);
    if(lsx-lix>lsy-liy)delmax=lsx-lix;else delmax=lsy-liy;
    fprintf(sai,"0 1\n0 1\n %d\n",count);    
    for(i=0;i<count;i++){
       xValues[i]=(xValues[i]-lix)/delmax;
       yValues[i]=(yValues[i]-liy)/delmax;    
       fprintf(sai,"%f %f\n",xValues[i],yValues[i]);               
    }      
    lix=liy=0; lsx=lsy=1;
    fclose(ent2);
    fclose(ent);  
    /*=========================================================================*/ 
    printf("simulations=%d\n",nsim);
    printf("casos=%d\n",nc);
    printf("pop=%d\n",count);
    fprintf(sai,"\n");
    
    
    count1=count-1;
    int kkq=0;    
    long nn,ii,jj,kk,kk0,kk1,mm,nlii,nlit,nnli,nndist;
    float x0,y0,x1,y1,x10,y10,z10;  
    long nv;
    float x2,y2;
    double dx,dy,demax,demin;
    int contt=0;
    double epp=1+1e-4;
start = clock();    
for(int kkk=0;kkk<nsim;kkk++){ //nsim simulacoes de Monte Carlo sob HA
    demax=0.0;demin=1e9;
    LLRmax=0.0;
    LLRmax2=0.0;
    LLRmax3=0.0;
    //tcmax = (int)fraccasmax*nc/100;
    printf(".");
    
    /*=========================================================================*/    
     
    if(fo45==0)for(i=0;i<nc;i++){for(j=0;j<nc;j++)micas[i][j]=0; micas[i][i]=1;}
    
    
    if(kkk>0){//sorteia nc casos       
      for(i=0;i<count;i++)icasa[i]=0;
      i=0;
      do{
        j=(int)(rnd(idum)*count);  
        if(icasa[j]==0){icasa[j]=1;icas[i]=j;i++;}       
      }while(i<nc);
    }    

    long it2=2,pins;
    nli=0;
    nnli=0;//contador para a lista da distancia euclidiana para os casos
    nndist=0;//contador para a distancia euclidiana para os casos
    for(i=0;i<nc;i++){
      //cout<<icas[i]<<" ";
      /*======================================================================*/                  
      if(fo45)goto o45;//salta para constru��o do grafo completo.
      /*======================================================================*/ 
                      
      //if(pf>1)fprintf(sai2,"\n"); 
      
      /*=======================================================================*/
      /*Construcao do grafo mais esparsso para extrair a �rvore gerado minima*/
      /*=======================================================================*/
      nlit=0;                                                      
      for(j=0;j<nc;j++){                  
          //if (j!=i) {
             lit[nlit][0]=icas[i];
             lit[nlit][1]=icas[j];
             //if(pf>1)fprintf(sai2,"i=%d j=%d  lit0=%d lit1=%d\n",i,j,lit[nlit][0],lit[nlit][1]);
             nlit++;
         // }
      }
      //if(pf>1)fprintf(sai2,"\n");  
      x0=xValues[icas[i]];
      //cout<<x0<<" "<<nlit<<"\n";;
      y0=yValues[icas[i]];
//      for(j=0;j<nlit;j++){
//        x1=xValues[lit[j][1]];
//        y1=yValues[lit[j][1]];        
//        x10=x1-x0;
//        y10=y1-y0;
//        z10=y10/x10;
//        if(fabs(x10)>1e-9){                           
//          if(z10>0)theta[j]=atan(y10/x10);//+pid2;
//          else theta[j]=atan(y10/x10)+pi+pid2;                
//        }else theta[j]=0.0;              
//        theta2[j]=theta[j];                   
//        itheta2[j]=idista[j]=indice[j]=j;
//        dista2[j]=dista[j]=x10*x10+y10*y10;
//        
//        //cout<<j<<" "<<x1<<" "<<theta[j]<<"\n";       
//      } 
      for(j=0;j<nlit;j++){
        x1=xValues[lit[j][1]];
        y1=yValues[lit[j][1]];        
        x10=x1-x0;
        y10=y1-y0;
        if(y10>0 && x10>0)theta[j]=atan(y10/x10);
        if(y10>0 && x10<0)theta[j]=atan(y10/x10)+pi;
        if(y10<0 && x10>0)theta[j]=atan(y10/x10)+pi+pi;
        if(y10<0 && x10<0)theta[j]=atan(y10/x10)+pi;
        if(y10==0 && x10==0)theta[j]=0.0;
        if(y10==0 && x10>0)theta[j]=0.000001;
        if(y10==0 && x10<0)theta[j]=pi;
        if(x10==0 && y10>0)theta[j]=pid2;
        if(x10==0 && y10<0)theta[j]=pi+pid2;   
        theta2[j]=theta[j];                   
        itheta2[j]=idista[j]=indice[j]=j;
        dista2[j]=dista[j]=x10*x10+y10*y10;
        //cout<<j<<" "<<x10<<" "<<theta[j]<<"\n";       
      }
      
            
      ordena4(0,nlit-1);//Ordena(cresc.) theta2[] acompanhado por dista2[], indexado por itheta2[]   
      ordena5(0,nlit-1);//Ordena(cresc.) dista[] acompanhado por theta[], indexado por idista[]   
      
      
      for(j=0;j<nc;j++){//*****
         idista2[j]=idista[j];      
         dista0[j]=dista[j];             
      } 
      //if(pf>1)fprintf(sai2,"\n");
      /*************************/
      thetan[0]=pi2; 
      thetan[1]=pi2;   
      ithetan[0]=0;
      ithetan[1]=1;
      for(jj=0;jj<nc;jj++){itheta[jj]=jj;} //***
      double tjj,djj; long itjj;
      entrop[0]=(theta[1]-theta[0])/pi2;
      entrop[1]=(theta[0]-theta[1]+pi2)/pi2;      
      deltaentrop=entropia2=-(entrop[0]*log(entrop[0])+entrop[1]*log(entrop[1]));      
      identrop[0]=0; dentrop[0]=1000;
      identrop[1]=1; dentrop[1]=deltaentrop;
      for(jj=2;jj<nc;jj++){//*****
         identrop[jj]=jj;    
         tjj=theta[jj]; itjj=itheta[jj]; djj=dista[jj]; indicejj=indice[jj];    
         long t0=0; long t00=jj/2; long t000=jj-1; 
         if(tjj>theta[t000])t00=t000;
         else do{//Faz bissecao.
           if(theta[t00]<tjj) {t0=t00; t00=(t00+t000)/2;}
                          else{t000=t00; t00=(t0+t00)/2;}
         }while(t000-t0>1);
         pins=t00;
         if(entrop[pins]<1e-12){entrop[pins]=1e-12;}
         deltaentrop=entrop[pins]*log(entrop[pins]);
        
         pins1=pins+1;
         for(k=jj;k>pins1;k--){//arreda
               k1=k-1;
               theta[k]=theta[k1]; 
               dista[k]=dista[k1];
               itheta[k]=itheta[k1];  
               entrop[k]=entrop[k1];              
         }              
         theta[pins1]=tjj; 
         dista[pins1]=djj;
         itheta[pins1]=itjj;  
         entrop[pins]=(theta[pins1]-theta[pins])/pi2;
         if(pins1==jj)entrop[pins1]=(theta[0]-theta[pins1]+pi2)/pi2;
         else entrop[pins1]=(theta[pins1+1]-theta[pins1])/pi2;
         
         jj1=jj+1;
         thetan[jj]=(theta[(pins1)%(jj1)]-theta[(jj+pins)%(jj1)]);   
         if(thetan[jj]<0){
            thetan[jj]+=pi2;
         }
         ithetan[jj]=jj;         
         entrop[jj]=(theta[0]-theta[jj]+pi2)/pi2; 
         if(entrop[pins1]<1e-12){entrop[pins1]=1e-12;}        
         deltaentrop-=entrop[pins]*log(entrop[pins]);
         deltaentrop-=entrop[pins1]*log(entrop[pins1]);
         entropia2+=deltaentrop;
         dentrop[jj]=deltaentrop;         
      }//for jj      

      ordena7(0,nc-1);//***                      
      
      //Seleciona por Pareto (max angulo versus max delta de entropia)
      // os (12? 10? 6?) melhores casos "proximos" de cada caso, para evitar 
      // ter que fazer o grafo completo de (n*(n-1))/2 arestas. 
      // Assim o grafo do qual ser� extra�do a arvore geradora minima
      // passa a ser um grafo mais esparso, #arestas=O(10n).  
      //descascamento
      //calcula Pareto-set      
      nlii=0;          
      kk=0;// kk � indice para o proximo elemento a entrar no Pareto-set
      kk0=0;// kk0 � o indice do primeiro elemento da camada atual do Pareto-set
      nn=1;//camada atual do Pareto-set
      mm=nc; f1[0]=thetan[0]; f2[0]=dentrop[0]; jj=ii=0;//****mm
      kk=1;// kk � indice para o proximo elemento a entrar no Pareto-set
      for(ii=0;ii<mm;ii++) {pareto[ii]=0; ipareto[ii]=ii;markpareto[ii]=0;}
      pareto[0]=1;
      do{
        for(ii=0;ii<mm;ii++){
          if(pareto[ii]==0 && fabs(dentrop[ii])>1e-12){
            jj=kk0;
            while( jj<kk && (f1[jj]<thetan[ii] || f2[jj]<dentrop[ii]) ) jj++;
            //compara com a camada atual do Pareto-set
            if(jj==kk){//acrescenta novo elemento ao Pareto-set.
              f1[kk]=thetan[ii];
              f2[kk]=dentrop[ii];
              pareto[ii]=nn;
              nlii++;  
              kk++;
            }
          }
        }
        nn++; 
        kk1=kk0;
        kk0=kk;
      }while(kk<mm);  // continue at� preencher pelo menos m pontos 
  
      
      fimpareto:;
      ordena8(0,nc-1);//*****
         
       
      
      nlii=0;      
      /*************************************************************************/                            
      kk=0;
      kke=0;
      while(kke<nord && kk<nc){  //****                            
        if( micas[i][idista2[kk]]==0 && markpareto[idista2[kk]]==0){                              
          markpareto[idista2[kk]]=1;
          li[nli][0]=icas[i];
          li[nli][1]=icas[idista2[kk]];
          li[nli][3]=i+1;
          li[nli][4]=idista2[kk]+1;
          micas[i][idista2[kk]]=1; 
          micas[idista2[kk]][i]=1;      
            
          nli++;  nlii++;
          kke++;
        }     
        kk++;
      }
         
      /*************************************************************************/          
      
      int pareto_antes,pareto_atual;
      kk=0;
      kke=0;
      npar0=npar;
      pareto_atual=pareto[0];
      while((kke<=npar0 || (pareto_antes==pareto_atual)) && kk<nc){   //***                      
        if(micas[i][indice[kk]]==0 && markpareto[indice[kk]]==0){                                        
          markpareto[indice[kk]]=1;                                    
          li[nli][0]=icas[i];
          li[nli][1]=icas[indice[kk]];
          li[nli][3]=i+1;
          li[nli][4]=indice[kk]+1;
          micas[i][indice[kk]]=1;
          micas[indice[kk]][i]=1;          
          nli++;  nlii++;
          kke++;
        }
        kk++;
        pareto_antes=pareto_atual;
        pareto_atual=pareto[kk];
      }  
      /*************************************************************************/
      // distancia euclidiana entre os casos
      
      for(j=i+1;j<nc;j++)
		{    
			dx=xValues[icas[j]]-xValues[icas[i]];
			dy=yValues[icas[j]]-yValues[icas[i]];
			distcas[i][j]=dy*dy+dx*dx;
						
			if (dy*dy+dx*dx < demin)demin=dy*dy+dx*dx;
			if (dy*dy+dx*dx > demax)demax=dy*dy+dx*dx;
			nndist++;
			
		}
        
        for(j=i+1;j<nc;j++)
		{    
			lide[nnli]=(distcas[i][j]-demin)/(demax-demin);
			nnli++;
		}
                 
      /*************************************************************************/                              
      //if(pf>1)fprintf(sai2,"nli=%d nlii=%d\n\n",nli,nlii);
      if(pf>1)cout<<"nlii="<<nlii<<endl;  
      fimforkk:;
      o45:;
      /*========================================================================*/
      /*Constu��o do Grafo completo*/
      /*========================================================================*/
      if(fo45){
        // distancia euclidiana entre os casos
        for(j=i+1;j<nc;j++){
            dx=xValues[icas[j]]-xValues[icas[i]];
	  		dy=yValues[icas[j]]-yValues[icas[i]];
	  		distcas[i][j]=dy*dy+dx*dx;
	  		if (dy*dy+dx*dx < demin)demin=dy*dy+dx*dx;
	  		if (dy*dy+dx*dx > demax)demax=dy*dy+dx*dx;
	   		nndist++;			
	  	}          
        for(j=i+1;j<nc;j++){                  
          li[nli][0]=icas[i];
          li[nli][1]=icas[j];
          li[nli][3]=i+1;
          li[nli][4]=j+1;
          lide[nli]=(distcas[i][j]-demin)/(demax-demin);		  
          //if(pf>1)fprintf(sai2,"%d %d: %d %d\n",i,j,li[nli][0],li[nli][1]);
          nli++;
        }
      /************************/
      }
      
    }  
    
    //cout<<kkk<<"  nli="<<nli<<"  ";
    //if(pff)fprintf(solucoes,"\n kkk=%d  nli=%d\n",kkk,nli);
   	//fclose(sai2);
   	
   	
   	/*=========================================================================*/   
    /*Construcao do diagrama de voronoi para casos/controles*/
    /*=========================================================================*/
    if(kkk==0)
    {   
          
    /*
    * The author of this software is Shane O'Sullivan.  
    * Permission to use, copy, modify, and distribute this software for any
    * purpose without fee is hereby granted, provided that this entire notice
    * is included in all copies of any software which is or includes a copy
    * or modification of this software and in all copies of the supporting
    * documentation for such software.
    * THIS SOFTWARE IS BEING PROVIDED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED
    * WARRANTY.  IN PARTICULAR, NEITHER THE AUTHORS NOR AT&T MAKE ANY
    * REPRESENTATION OR WARRANTY OF ANY KIND CONCERNING THE MERCHANTABILITY
    * OF THIS SOFTWARE OR ITS FITNESS FOR ANY PARTICULAR PURPOSE.
    */          
	VoronoiDiagramGenerator vdg;
	vdg.generateVoronoi(xValues,yValues,count, lix,lsx,liy,lsy,0);
	vdg.resetIterator();
	

	//printf(".");
	nv=0;
	//Escreve segmentos de Voronoi
	while(vdg.getNext(x1,y1,x2,y2))
	{
		if(ff)fprintf(sai,"%f %f %f %f\n",x1,y1,x2,y2);
		voronoi[nv][0]=x1;
		voronoi[nv][1]=y1;
		voronoi[nv][2]=x2;
		voronoi[nv][3]=y2;
		nv++;
	}
   /*	End of part made by Shane O'Sullivan's software.*/
   }//fim if kkk==0
   /*==========================================================================*/
    /*Sa�da cont�m: limites do ret�ngulo do dom�nio,#pontos,pontos,segmentos de Voronoi.*/
	if(ff)fprintf(sai,"200 200 200 200\n");//sinaliza fim de lista de voronoi
	if(ff)fprintf(sai,"nv=%d\n",nv); 
	
     
    /*=========================================================================*/
    /*Calcula interse��es dos (n-1)*(n-1) segmentos que unem os n pontos de casos 
    aos segmentos do diagrama de Voronoi.*/
    /*=========================================================================*/
     
    if(ff)vor=fopen("vor.txt","w"); 
    //goto fimk0;
    
    for(i=0;i<nv;i++){
       x1=voronoi[i][0];       
       x2=voronoi[i][2];         
       if(x2<x1){xminv=x2;xmaxv=x1;}else {xminv=x1;xmaxv=x2;}
       xxx[i]=xmaxv;
       yyy[i]=xminv;
       rxxx[i]=i;       
    }
    ordenaxxx(0,nv-1);     
    
    fimk0:;
    //goto fimk;
    long contxy=0,contxy2=0,contxy3=0;
    for(j=0;j<nli;j++){
       ni=0;               
       x3=xValues[li[j][0]];
       y3=yValues[li[j][0]];      
       x4=xValues[li[j][1]];
       y4=yValues[li[j][1]];      
       x43=x4-x3;
       y43=y4-y3;
       if(x43<0){xmina=x4;xmaxa=x3;}else {xmina=x3;xmaxa=x4;}
       if(y43<0){ymina=y4;ymaxa=y3;}else {ymina=y3;ymaxa=y4;}
       long vor00=0; long vor000=nv-1; long vor0=nv/2;
       do{//Faz bissecao.
         if(xxx[vor0]<xmina) {vor00=vor0; vor0=(vor0+vor000)/2;}
         else{vor000=vor0; vor0=(vor00+vor0)/2;}
       }while(vor000-vor00>1);
       vor0+=1;  
       long vor1=nv-1;   
       for(i=vor0;i<=vor1;i++)if(yyy[i]<xmaxa){  //contxy++;                                        
         x1=voronoi[rxxx[i]][0];
         y1=voronoi[rxxx[i]][1];
         x2=voronoi[rxxx[i]][2];
         y2=voronoi[rxxx[i]][3];     
         y21=y2-y1;
         if(y21<0){yminv=y2;ymaxv=y1;}else {yminv=y1;ymaxv=y2;}
         if(ymaxa>=yminv)if(ymaxv>=ymina){
           //contxy2++;                                
           x21=x2-x1;          
 	       //fprintf(vor,"i=%d  x1=%f y1=%f x2=%f y2=%f\n",i,x1,y1,x2,y2);  
 	       x13=x1-x3;
 	       y13=y1-y3;
           den=y43*x21-x43*y21;
           if(fabs(den)>1e-9){
              ua=(x43*y13-y43*x13)/den;
              if(ua>=-1e-9)if(ua<=epp){
              //if(ua>=0)if(ua<=1){              
                ub=(x21*y13-y21*x13)/den;                 
                if(ub>=-1e-9)if(ub<=epp){
                //if(ub>=0)if(ub<=1){     
                  ni++; //contxy3++;
                  if(ff){
                    x=x3+ub*x43;
                    y=y3+ub*y43;       
                    fprintf(vor,"%f %f\n",x,y);
                  }
                } 
              }  
           }
         }//
       }
       if(ff)fprintf(vor,"%f %f\n",200.0,200.0);
       li[j][2]=ni;
       lide[j]+=li[j][2];//termo de desempate para as arestas de igual peso
       //if(ff)fprintf(sai,"%d %d %d: %d\n",j,li[j][0],li[j][1],li[j][2]);  
       if(ff)fprintf(sai,"%f\n",lide[j]);         
    }
    if(ff)fprintf(vor,"%f %f\n",0.0,0.0);
    if(ff)fprintf(vor,"%f %f\n",1000.0,1000.0);
    fimk:;
    
    /*=========================================================================*/
    
    if(ff)fclose(sai);
    
    /*=========================================================================*/
    /*�rvore Geradora M�nima*/
    /*=========================================================================*/
    int econt, totalcusto;
	int k, custo;
	int i;
	
	k=LeArestasVetorli();
    //Inicializa classes.
	for (i = 1; i <= n; i++){
		vertice[i] = i;
    }	
    totalcusto=ArvoreGM(k);
        
	display(totalcusto);
	
	//Calcula grau de cada vertice da arvore
	for (i = 1; i <= n; i++){
		qvertice[i]=0;
    }
    for (i = 1; i < n; i++){
       qvertice[Arvore[i][0]]++;
       qvertice[Arvore[i][1]]++;
    }
    if(fmediaarestas==1){
//      if(kkk==0)fprintf(so,"\nSoma e m�dia das arestas, e grau  em cada nodo:\n");
      for (i = 1; i <= n; i++){
        mediaarestas[i]=somaarestas[i]/(float)qvertice[i];
//        if(kkk==0)fprintf(so,"i=%d: s=%d med=%f min=%f =%d\n",i,somaarestas[i],mediaarestas[i],minarestas[i],qvertice[i]);  
      }    
    }
    else if(fmediaarestas==0){
//      if(kkk==0)fprintf(so,"\nSoma e m�nimo das arestas, e grau  em cada nodo:\n");
      for (i = 1; i <= n; i++){
        mediaarestas[i]=minarestas[i];  
//        if(kkk==0)fprintf(so,"%d: %d %f %d\n",i,somaarestas[i],mediaarestas[i],qvertice[i]);  
      }    
    }
    else if(fmediaarestas==2){
//      if(kkk==0)fprintf(so,"\nSoma e m�ximo das arestas, e grau  em cada nodo:\n");
      for (i = 1; i <= n; i++){
        mediaarestas[i]=maxarestas[i];  
//        if(kkk==0)fprintf(so,"%d: %d %f %d\n",i,somaarestas[i],mediaarestas[i],qvertice[i]);  
      }    
    }
    else if(fmediaarestas==3){
//      if(kkk==0)fprintf(so,"\nSoma e m�ximo das arestas, e grau  em cada nodo:\n");
      for (i = 1; i <= n; i++){
        if(qvertice[i]>2)mediaarestas[i]=(somaarestas[i]-maxarestas[i]-minarestas[i])/(qvertice[i]-2.0);
        else mediaarestas[i]=somaarestas[i]/(float)qvertice[i];  
//        if(kkk==0)fprintf(so,"%d: %d %f %d\n",i,somaarestas[i],mediaarestas[i],qvertice[i]);  
      }    
    }
    else if(fmediaarestas==4){
//      if(kkk==0)fprintf(so,"\nSoma e m�ximo das arestas, e grau  em cada nodo:\n");
      for (i = 1; i <= n; i++){
        if(qvertice[i]>2)mediaarestas[i]=(somaarestas[i]-maxarestas[i]-minarestas[i])/(qvertice[i]-2.0);
        else mediaarestas[i]=minarestas[i];   
//        if(kkk==0)fprintf(so,"%d: %d %f %d\n",i,somaarestas[i],mediaarestas[i],qvertice[i]);  
      }    
    }
    if(kkk==0)fprintf(so,"\n");
 //   if(pf>1){
//       cout<<"Grau:"<<endl; if(kkk==0)fprintf(so,"Grau:\n");
//       for (i = 1; i < n; i++){
//         cout<<qvertice[i]<<":"; if(kkk==0)fprintf(so,"%d:",qvertice[i]);
//       }  
//       cout<<endl<<endl; if(kkk==0)fprintf(so,"\n\n");	
//       cout<<"Construcao dos clusters por adicao de arestas:"<<endl<<endl;
//       if(kkk==0)fprintf(so,"Construcao dos clusters por adicao de arestas:\n\n");
//    }    
    /*========================================================================*/
    
    /*=========================================================================*/
    /*Constroi clusters*/
    /*=========================================================================*/
    ncl=0;
    
    int ccluster=0;
    for (i = 1; i <= n; i++)
		cvertice[i]=0;
    for (i = 1; i < n; i++){
      int cv0=cvertice[Arvore[i][0]];  
      int cv1=cvertice[Arvore[i][1]];  
      if(cv0==0 && cv1==0){ 
        ncluster[ncl]=0;    
        cluster[ncl][ncluster[ncl]]=Arvore[i][0];
        ncluster[ncl]++;
        cluster[ncl][ncluster[ncl]]=Arvore[i][1];
        ncluster[ncl]++;                    
        ccluster++; 
        cvertice[Arvore[i][0]]=ccluster;
        cvertice[Arvore[i][1]]=ccluster;    
        asoma[Arvore[i][0]]=Arvore[i][2];//Soma acumulada dos pesos das arestas.
        asoma[Arvore[i][1]]=Arvore[i][2];//Soma acumulada dos pesos das arestas.
        scluster[ncl]=Arvore[i][2]; 
        if(pf>1){
           cout<<"+ ";
           cout<<Arvore[i][0]<<" "<<Arvore[i][1];
           cout<<"   s="<<scluster[ncl]<<endl;
        }     
        //fprintf(so,"+ %d %d s=%d\n",Arvore[i][0],Arvore[i][1],scluster[ncl]);
        ncl++; 
      }
      else{//cv0>0 || cv1>0 
          if(cv0>0 && cv1>0){
            //ccluster--;  
            if(pf>1){cout<<"- ";}
            //fprintf(so,"- ");                          
            if(cv0 < cv1){ 
              int asa0=asoma[Arvore[i][0]];
              int asa1=asoma[Arvore[i][1]];
              for(j=1;j<=n;j++){ 
                if(cvertice[j]==cv1){
                   cvertice[j]=cv0;
                   //cout<<j<<"*"<<asoma[j]<<"+="<< Arvore[i][2]<<"+"<<asa0 <<endl;
                   asoma[j]=(asa1+Arvore[i][2]+asa0);//Soma acumulada dos pesos das arestas. 
                   //cout<<asoma[j]<<endl;
                }
                else if(cvertice[j]==cv0){
                   //cvertice[j]=cv1;  
                   asoma[j]=(asa0+Arvore[i][2]+asa1);//Soma acumulada dos pesos das arestas. 
                }   
              }
              scluster[ncl]=asa0+asa1+Arvore[i][2];  
              ncluster[ncl]=0;
              for(j=1;j<=n;j++){ 
                   if(cvertice[j]==cv0){ 
                     if(pf>1)cout<<j<<" "; 
                     //fprintf(so,"%d ",j);
                     cluster[ncl][ncluster[ncl]]=j;
                     ncluster[ncl]++;
                   }   
              }
              if(pf>1){cout<<" s="<<scluster[ncl]<<endl;}
              //fprintf(so," s=%d\n",scluster[ncl]);
              ncl++;    
            }//cv0<cv1
            else{//cv0>=cv1
              int asa0=asoma[Arvore[i][0]];
              int asa1=asoma[Arvore[i][1]]; 
              for(j=1;j<=n;j++){ 
                if(cvertice[j]==cv0){ 
                   cvertice[j]=cv1;
                   asoma[j]=(asa0+Arvore[i][2]+asa1);//Soma acumulada dos pesos das arestas.   
                }
                else if(cvertice[j]==cv1){
                   //cvertice[j]=cv0;  
                   asoma[j]=(asa1+Arvore[i][2]+asa0);//Soma acumulada dos pesos das arestas.  
                }            
              }
              scluster[ncl]=asa0+asa1+Arvore[i][2];   
              ncluster[ncl]=0;
              for(j=1;j<=n;j++){ 
                   if(cvertice[j]==cv1){ 
                     if(pf>1)cout<<j<<" ";
                     //fprintf(so,"%d ",j);
                     cluster[ncl][ncluster[ncl]]=j;
                     ncluster[ncl]++;
                   }   
              }   
              if(pf>1){cout<<" s="<<scluster[ncl]<<endl;}
              //fprintf(so," s=%d\n",scluster[ncl]);
              ncl++;      
            }//cv0>=cv1                                                
          }
          else{//cv0==0 || cv1==0
            int asa0=asoma[Arvore[i][0]];
            int asa1=asoma[Arvore[i][1]];
            ccluster=ccluster;  if(pf>1)cout<<". ";   
            //fprintf(so,". ");
            if(cv0==0){
                  cvertice[Arvore[i][0]]=cv1;
                  asoma[Arvore[i][0]]=asa1;
                  ncluster[ncl]=0;
                  for(j=1;j<=n;j++){ 
                    if(cvertice[j]==cv1){ 
                      if(pf>1){cout<<j<<" ";}
                      //fprintf(so,"%d ",j);
                      asoma[j]+=Arvore[i][2];//Soma acumulada dos pesos das arestas.  
                      cluster[ncl][ncluster[ncl]]=j;
                      ncluster[ncl]++;
                    }   
                  }
                  scluster[ncl]=asa1+Arvore[i][2]; 
                  if(pf>1){cout<<" s="<<scluster[ncl]<<endl;}
                  //fprintf(so," s=%d\n",scluster[ncl]);
                  ncl++;
            }//cv0==0           
            else {//cv1==0
                 cvertice[Arvore[i][1]]=cv0; 
                 asoma[Arvore[i][1]]=asa0; 
                 ncluster[ncl]=0;
                 for(j=1;j<=n;j++){ 
                   if(cvertice[j]==cv0){ 
                     if(pf>1)cout<<j<<" ";
                     //fprintf(so,"%d ",j);
                     asoma[j]+=Arvore[i][2];//Soma acumulada dos pesos das arestas. 
                     cluster[ncl][ncluster[ncl]]=j;
                     ncluster[ncl]++;
                   }   
                 }
                 scluster[ncl]=asa0+Arvore[i][2]; 
                 if(pf>1){cout<<" s="<<scluster[ncl]<<endl;}
                 //fprintf(so," s=%d\n",scluster[ncl]);
                 //<<"="<<asa0<<"+"<<Arvore[i][2]<<endl;
                 ncl++;
             }//cv1==0
          }   
      }//fim else  (cv0>0 || cv1>0) 
 //     for (j=1; j<=n; j++)
//        {if(pf>1)cout<<cvertice[j]<<":";fprintf(so,"%d:",cvertice[j]);}
//      if(pf>1){cout<<" "<<Arvore[i][0]<<"-"<<Arvore[i][1]<<endl;}
      //fprintf(so," %d-%d\n",Arvore[i][0],Arvore[i][1]);
      //if(pf)cout<<endl;
    }//for i 
    /*========================================================================*/          
    
     
    /*=========================================================================*/
    /*C�lculo da verossimilhanca para os clusters candidatos*/
    /*=========================================================================*/     
    if(pf>1){cout<<endl;}
    if(pff){
      //fprintf(so,"Clusters:\n\n");       
      for (i=0; i<ncl; i++){        
        //fprintf(solucoes,"%d   %d  ",scluster[i],ncluster[i]);        
        popscan=0.0; // populacao do cluster        
        for (j=0; j<ncluster[i]; j++){           
          popscan+=g(mediaarestas[cluster[i][j]]);//obs: indexa��o correta = indexa�ao da Arvore[i]           
        }        
        popscan*=(pi/4.0);        
        casscan=ncluster[i]; // # casos no cluster
        if (casscan<=nc*fraccasmax/100){ 
        scan = poisson(casscan,popscan,nc,count);
        //scan = poisson(cz[i][j],popz[i][j][e][a],C,N);               
        mu=nc*(popscan/count);//# casos esperados no cluster
 //       if(casscan>mu)
//          scan=(casscan)*log(casscan/mu)+(nc-casscan)*log((nc-casscan)/(nc-mu));
//        else scan=0.0;        
          if(scan>LLRmax){ //encontrando a estatistica de teste.
				LLRmax=scan;
				iLLRmax=i;
	            if(kkk==0){
                          fprintf(so,"location=%d pop=%f cas=%f E[cas]=%f LLRmax=%f : ",i+1,popscan,casscan,mu,scan);
                          for (jj=0; jj<ncluster[iLLRmax]; jj++)
                              fprintf(so," %d",icas[cluster[iLLRmax][jj]-1]+1);
                         }
                         fprintf(so,"\n");
			} 
            if(scan>LLRmax2 && scan<LLRmax){ //encontrando a estatistica de teste.
				LLRmax2=scan;
				iLLRmax2=i;
	            if(kkk==0){
                          fprintf(so2,"location=%d pop=%f cas=%f E[cas]=%f LLRmax=%f : ",i+1,popscan,casscan,mu,scan);
                          for (jj=0; jj<ncluster[iLLRmax2]; jj++)
                              fprintf(so2," %d",icas[cluster[iLLRmax2][jj]-1]+1);
                         }
                         fprintf(so2,"\n");
			}
            if(scan>LLRmax3 && scan<LLRmax2){ //encontrando a estatistica de teste.
				LLRmax3=scan;
				iLLRmax3=i;
	            if(kkk==0){
                          fprintf(so3,"location=%d pop=%f cas=%f E[cas]=%f LLRmax=%f : ",i+1,popscan,casscan,mu,scan);
                          for (jj=0; jj<ncluster[iLLRmax3]; jj++)
                              fprintf(so3," %d",icas[cluster[iLLRmax3][jj]-1]+1);
                         }
                         fprintf(so3,"\n");
			}       
          //fprintf(so,"  mu=%f pop=%f cas=%f scan=%f   ",mu,popscan,casscan,scan);          
        }
        //for (j=0; j<ncluster[i]; j++){          
          //fprintf(solucoes," %d",cluster[i][j]-1);//obs: ordem de 0 a #casos-1
          //fprintf(so," %d",cluster[i][j]-1);//obs: ordem de 0 a #casos-1
        //}   
        //fprintf(so," :: %d  %d",scluster[i],ncluster[i]);
        //fprintf(so,"\n");
        //fprintf(solucoes,"\n");
      }
      /*=======================================================================*/
      //fprintf(solucoes,"\n -1");
      fprintf(sof,"%f  ",LLRmax);
      //fprintf(so,"-1 \n");
      if(kkk==0)fprintf(sai3,"%d\n",ncluster[iLLRmax]);
      for (j=0; j<ncluster[iLLRmax]; j++){
          fprintf(sof," %d",icas[cluster[iLLRmax][j]-1]+1);// de acordo com o arquivo de entrada
          if(kkk==0)fprintf(sai3,"%f  %f \n",xValues[icas[cluster[iLLRmax][j]-1]],yValues[icas[cluster[iLLRmax][j]-1]]);
      }         
      fprintf(sof,"\n");   
    }   
    if(ff)fclose(vor);
    ff=0;
    
    /*=======================================================================*/
    //calculo P-valor
    /*=======================================================================*/
    if(kkk==0)LLRmax0=LLRmax;
    else if(LLRmax>=LLRmax0)sig++;     

	/*=======================================================================*/
    
    if(ftg){
       fftg=fopen("fftg.txt","w");
       for(i=0;i<nc;i++){ 
   	      fprintf(fftg,"-1 %d 0 0\n",i);
   	      fprintf(fftg,"-2 0 0 0\n"); 
   	      fprintf(fftg,"%d %d %d %d\n",icas[i],icas[i],i+1,i+1);
   	      ii=0;
          for(kk=0;kk<nc;kk++)     
            if(kk!=i)if(micas[i][kk]){ 
   	          fprintf(fftg,"%d %d %d %d\n",icas[i],icas[kk],i+1,kk+1);
   	          ii++;
            }  
          fprintf(fftg,"-3 %d 0 0\n\n",ii);
       }        
   	   fclose(fftg);
    }
    
    fprintf(sai4," %f\n",(sig+1)/(kkk+1));
    if(kkk>0)cout<<(sig+1)/(kkk+1)<<"\n";  
    LLRcrit[kkk]=LLRmax;
}//for kkk	
end = clock();
cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

fprintf(sai4,"\n ==================\n");
fprintf(sai4,"\n P-valor=%f\n",(sig+1)/nsim);
fprintf(sai4,"\n ==================\n");
fprintf(sai4,"\n Cpu-Time=%f",cpu_time_used);
fprintf(sai4,"\n sign=%f",sig);
jsim=(0.95*nsim);
j=(int)jsim-1;
ordenallr(0,nsim-1);
for(i=0;i<nsim;i++)fprintf(llrcrit," %f\n",LLRcrit[i]);
    fprintf(llrcrit,"\n ==================\n");
    fprintf(llrcrit,"\n LLRcrit=%f\n",LLRcrit[j]);
    fprintf(llrcrit,"\n ==================\n");


fclose(sai3);
if(pff)fclose(solucoes);
fclose(so);
fclose(so2);
fclose(so3);
fclose(sof);//solucao final
fclose(sai4);


fimprog:;
	system("PAUSE");    
	return 0;	
}
     

float rnd(long idum0){
   long k;
   float ans;
   do{
     idum0 ^= MASK;
     k=(idum0)/IQ;
     idum0=IA*(idum0-k*IQ)-IR*k;
     if(idum0<0) idum0+=IM;
     ans=AM*(idum0);
     idum0^=MASK;
     idum=idum0;
   }while(ans>0.999999);
   return(ans);
}


